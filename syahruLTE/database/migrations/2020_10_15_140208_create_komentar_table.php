<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomentarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar', function (Blueprint $table) {
            $table->bigIncrements('id_komentar');

            $table->unsignedBigInteger('id_jawaban');
            $table->foreign('id_jawaban')->references('id_jawaban')->on('jawaban')->onDelete('cascade');;

            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id_user')->on('users')->onDelete('cascade');;

            $table->text('isi');
            $table->dateTime('tanggal_dibuat',0);
            $table->dateTime('tanggal_diperbaharui',0)->nullable();
            $table->integer('jumlah_like')->nullable();
            $table->integer('jumlah_dislike')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar');
    }
}
