<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>

<?php
//echo Form::open(array('action' => 'AuthController@welcome'))
?>
<form action="/welcome" method="post">
@csrf
    <p>First Name:</p>
    <input type="text" name="first" id="first" >

    <br>

    <p>Last Name:</p>
    <input type="text" name="last" id="last">

    <br>

    <p>Gender:</p>
    <input type="radio" id="male" name="gender" value="male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label for="other">Other</label>

    <br>

    <p>Nationality:</p>
    <select name="nationality" id="nationality">
        <option value="indonesian" selected>Indonesian</option>
        <option value="singapuran" selected>Singaporean</option>
        <option value="malaysian" selected>Malaysian</option>
    </select>

    <br>
    
    <p>Language Spoken:</p>
    <input type="checkbox" id="bahasa1" name="bahasa1" value="bahasa_indonesia">
    <label for="bahasa1"> Bahasa Indonesia</label><br>
    <input type="checkbox" id="bahasa2" name="bahasa2" value="english">
    <label for="bahasa2"> English</label><br>
    <input type="checkbox" id="bahasa3" name="bahasa3" value="other">
    <label for="bahasa3"> Other</label><br>

    <br>

    <p>Bio:</p>
    <textarea id="bio" name="bio" cols="30" rows="15"></textarea> 

    <br>
    <br>
    
    <button type="submit">Sign Up</button>

</form>