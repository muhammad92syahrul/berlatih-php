<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    //
    public function create(){
        return view('pertanyaan.create');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function store(Request $request)
    {
        die(var_dump($request['judul']));
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan');
    }

    public function show($id_pertanyaan){
        $pertanyaan = DB::table('pertanyaan')->where('id_pertanyaan', $id_pertanyaan)->first();
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id_pertanyaan){
        $pertanyaan = DB::table('pertanyaan')->where('id_pertanyaan', $id_pertanyaan)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id_pertanyaan, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        $query = DB::table('pertanyaan')
            ->where('id_pertanyaan', $id_pertanyaan)
            ->update([
                'judul' => $request["judul"],
                'isi' => $request["isi"]
            ]);
        return redirect('/pertanyaan');
    }

    public function destroy($id_pertanyaan){
        $query = DB::table('pertanyaan')->where('id_pertanyaan', $id_pertanyaan)->delete();
        return redirect('/pertanyaan');
    }
}
