@extends('master')

@section('contents')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Buat Pertanyaan Baru</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" method="post" action="/pertanyaan">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Judul</label>
          <input type="text" class="form-control" id="judul" placeholder="Tulis judul">
          @error('title')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group">
            <label>Jenis Pertanyaan</label>
            <select class="form-control">
              <option>Bercanda</option>
              <option>Serius</option>
            </select>
          </div>
        <div class="form-group">
            <label for="isi">Isi</label>
            <textarea class="form-control" id="isi" cols=20 rows=5>
            </textarea>
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
@endsection