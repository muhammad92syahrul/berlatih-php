<?php

function hitung($string_data){
    $operator=["*"=>'kali',":"=>'bagi',"-"=>'kurang',"+"=>'tambah',"%"=>'sisa'];
    $hasil;
    $digit="";
    for($i=0;$i<strlen($string_data);$i++){
        if(is_numeric($string_data[$i])){
            $digit.=$string_data[$i];
        }else{
            $operan=$string_data[$i];
            $digit_satu=(int)$digit;
            $digit="";
        }
    }
    $digit_dua=(int)$digit;

    if($operator[$operan]=='kali'){
        $hasil= $digit_satu*$digit_dua."<br>";
    }else if($operator[$operan]=='bagi'){
        $hasil= $digit_satu/$digit_dua."<br>";
    }else if($operator[$operan]=='kurang'){
        $hasil= $digit_satu-$digit_dua."<br>";
    }else if($operator[$operan]=='tambah'){
        $hasil= $digit_satu+$digit_dua."<br>";
    }else if($operator[$operan]=='sisa'){
        $hasil= $digit_satu%$digit_dua."<br>";
    }else{
        $hasil= "<br>";
    }
    return $hasil;
}


echo hitung("102*2");
echo hitung("2+3");
echo hitung("100:25");
echo hitung("10%2");
echo hitung("99-2");
