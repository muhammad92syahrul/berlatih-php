
<?php
require_once("animal.php");
require_once("Frog.php");
require_once("Ape.php");

$sheep = new Animal("shaun");

echo $sheep->name."<br>"; // "shaun"
echo $sheep->legs."<br>"; // 2
echo $sheep->cold_blooded."<br>"; // false

$sungokong = new Ape("kera sakti");
// print_r($sungokong);
$sungokong->yell(); // "Auooo"
echo "<br>";

$kodok = new Frog("buduk");
// print_r($kodok);
$kodok->jump()."<br>" ; // "hop hop"
echo "<br>";

?>
