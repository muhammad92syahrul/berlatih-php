<?php
class Animal {
    
    public $legs;
    public $name;
    public $cold_blooded;
    public function __construct($name) 
    {
      $this->name=$name;
      $this->legs=2;
      $this->cold_blooded='false';
    }

  }

?>