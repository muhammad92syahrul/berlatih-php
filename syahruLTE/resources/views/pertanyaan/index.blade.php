@extends('master')
@section('contents')
<a href="/pertanyaan/create" class="btn btn-primary">Tambah</a>
<br>
<br>
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">


        <table id="example1" class="table table-bordered table-striped">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Isi</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($pertanyaan as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->isi}}</td>
                        <td>
                            <a href="/posts/{{$value->id_pertanyaan}}" class="btn btn-info">Show</a>
                            <a href="/posts/{{$value->id_pertanyaan}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/posts/{{$value->id_pertanyaan}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr >
                        <td colspan="4">No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        {{-- <a href="#" class="card-link">Card link</a> --}}
    </div>
    <!-- /.card-body -->
  </div>
@endsection

@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
$(function () {
  $("#example1").DataTable();
});
</script>
@endpush