

CREATE TABLE customers (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE orders (
  id int(11) NOT NULL AUTO_INCREMENT,
  amount varchar(255) NOT NULL,
  customer_id int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (customer_id) REFERENCES customers(id)
);




