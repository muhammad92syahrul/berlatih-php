<div>
    <h2>Edit Post {{$pertanyaan->id}}</h2>
    <form action="/pertanyaan/{{$pertanyaan->id_pertanyaan}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="judul">Title</label>
            <input type="text" class="form-control" name="judul" value="{{$pertanyaan->judul}}" id="judul" placeholder="Masukkan Judul">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="isi">body</label>
            <input type="text" class="form-control" name="isi"  value="{{$pertanyaan->isi}}"  id="body" placeholder="Masukkan Isi">
            @error('isi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>